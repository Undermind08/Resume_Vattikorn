<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous">
    </script>
    <style>
    @import 'https://fonts.googleapis.com/css?family=Kanit|Prompt'; /* >>>>> font google */
    .inner_position_top{
        position:absolute;
        display:block;
        height:50px; /* กำหนดความสูงส่วนของเนื้อหาที่นำมาซ้อนทับ */
        width:100%; /* กำหนดความกว้างของเนื้อหาที่นำมาซ้อนทับ แบบขยายเต็ม */
        center:0px; /* css กำหนดชิดด้านบน  */
        z-index:999;
        margin-top: -12px;}
    .range-slider__range {
        -webkit-appearance: none;
        width: calc(100% - (73px));
        height: 10px;
        border-radius: 5px;
        background: #d7dcdf;
        outline: none;
        padding: 0;
        margin: 0;
    }
    asdasd

    /* custom thumb */
    .range-slider__range::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        background: #2c3e50;
        cursor: pointer;
        -webkit-transition: background .15s ease-in-out;
        transition: background .15s ease-in-out;
    }

    .range-slider__range::-webkit-slider-thumb:hover {
        background: #1abc9c;
    }

    .range-slider__range:active::-webkit-slider-thumb {
        background: #1abc9c;
    }

    .range-slider__range::-moz-range-thumb {
        width: 20px;
        height: 20px;
        border: 0;
        border-radius: 50%;
        background: #2c3e50;
        cursor: pointer;
        -webkit-transition: background .15s ease-in-out;
        transition: background .15s ease-in-out;
    }

    .range-slider__range::-moz-range-thumb:hover {
        background: #1abc9c;
    }

    .range-slider__range:active::-moz-range-thumb {
        background: #1abc9c;
    }

    .range-slider__range:focus::-webkit-slider-thumb {
        -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 6px #1abc9c;
        box-shadow: 0 0 0 3px #fff, 0 0 0 6px #1abc9c;
    }

    /* custom label */
    .range-slider__value {
        display: inline-block;
        position: relative;
        width: 60px;
        color: #fff;
        line-height: 20px;
        text-align: center;
        border-radius: 3px;
        background: #2c3e50;
        padding: 5px 10px;
        margin-left: 8px;
    }

    .range-slider__value:after {
        position: absolute;
        top: 8px;
        left: -7px;
        width: 0;
        height: 0;
        border-top: 7px solid transparent;
        border-right: 7px solid #2c3e50;
        border-bottom: 7px solid transparent;
        content: '';
    }

    /* custom track */
    ::-moz-range-track {
        background: #d7dcdf;
        border: 0;
    }

    /* remove border */
    input::-moz-focus-inner, input::-moz-focus-outer {
        border: 0;
    }
    #rcorners7 {
        border-radius: 50px/15px;
        background: #ffffff;
        padding: 20px;
        width: 1000px;
        height: 100%;
        display: block;
        margin-left: auto;
        margin-right: auto;
        box-shadow:6px 3px 100px #888888;
    }

    #rcorners8 {
        border-radius: 15px/50px;
        background: #73AD21;
        padding: 20px;
        width: 200px;
        height: 150px;
    }

    #rcorners9 {
        border-radius: 50%;
        background: #73AD21;
        padding: 20px;
        width: 200px;
        height: 150px;
    }
    </style>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    <title>Resume Vattikorn Prajonkbua</title>
</head>
<body style="background-color:#fff7e6;">

<div class="header">
    <div id="rcorners7">

    <div class="container" style="margin-right: -50px;">
        <div class="row">
            <div class="col-md-5">
                <div class="col-sm-3" style="width: 100%">
                    <img src="img/app.png" style="width: 60%; height: 60%;">
                    <h1>วัทธิกร ประจงบัว</h1><hr>
                    <div class="inner_position_top">
                        <h3> <font color="red">Web  Developer </font> </h3>
                    </div><br><br>
                    <h2 style="font-variant: normal; width: 100%; height: 100%;background-color: #413F3D; color:white;margin-top:30px;">&nbsp;ข้อมูลส่วนตัว</h2>

                    <div><img src="img/user_icon2.png" width="5%" height="5%">&nbsp;   นายวัทธิกร ประจงบัว </div><hr>
                    <div><img src="img/icon_adress.png" width="5%" height="5%">&nbsp; 59/16 ถ.นิคมลำตะคอง ซ.นิคมลำตะคอง1   ต.หนองสาหร่าย อ.ปากช่อง จ.นครราชสีมา</div><hr>
                    <div><img src="img/icon_phone.png" width="5%" height="5%">&nbsp; 091-5416351 </div><hr>
                    <div><img src="img/icon_email.png" width="5%" height="5%">&nbsp; vattikornnook@gmail.com </div><hr>
                    <div><img src="img/icon_facebook.png" width="5%" height="5%">&nbsp; www.facebook.com/nuk.vattikorn </div><hr>
                    <div><img src="img/icon_line.png" width="5%" height="5%">&nbsp; nukundermind</div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="col-md-5">

                </div>

                <div class="col-sm-9">
                    <h2 style="margin-top: 10%;font-variant: normal; width: 100%; height: 100%;background-color: #413F3D; color:white;">&nbsp;เป้าหมายในการทำงาน</h2>

                    <h4><small style="font-size: 15px;text-align:justify;">เพื่อประสบความสำเร็จในวิชาชีพและต้องการใช้ความรู้ทักษะเรียนรู้ การทำงานขององค์กรเพื่อพัฒนาศักยภาพ
                        มีความสนใจในเทคโนโลยีพร้อมเรียนรู้และเปิดรับสิ่งใหม่ๆอยู่เสมอ
                        </small></h4>

                    <h2 style="font-variant: normal; width: 100%; height: 100%;background-color: #413F3D; color:white;">&nbsp;การศึกษา</h2>

                    <h4><small style="font-size: 15px;">
                            <p><b>มัทธยมศึกษาตอนปลาย:</b> โรงเรียนปากช่อง<br><b>ปัจจุบัน:</b> นักศึกษาชั้นปีที่ 4 คณะวิทยาศาสตร์ <br>
                                <b>สาขา</b>เทคโนโลยีสาระสนเทศ (Information Technology)<br>
                                มหาวิทยาลัยราชภัฏเชียงใหม่
                            </p>


                        </small></h4>

                    <h2 style="font-variant: normal; width: 100%; height: 100%;background-color: #413F3D; color:white;">&nbsp;ประสบการณ์ทำงาน</h2>

                    <h4><small style="font-size: 15px;">
                            <p><b> 30 พฤศจิกายน 2560 - 28 กุมภาพันธ์ 61  </b><br>
                                - ฝึกประสบการณ์การทำงาน <br> &nbsp;&nbsp;&nbsp; บริษัท ธารา(อีซูซุ) จำกัด 99/10 หมู่ 5 ถ.เชียงใหม่-ดอยสะเก็ด ต.หนองป่าครั่ง อ.เมือง จ.เชียงใหม่<br>
                                - ตำแหน่ง<br>
                                &nbsp;&nbsp;&nbsp;Web Programmer <br>
                                - รายละเอียดงาน <br>
                                &nbsp;&nbsp;&nbsp; 1. วิเคราะห์และออกแบบระบบการจัดการโรงเคาะพ่นสีบริษัทธาราจำกัด<br>
                                &nbsp;&nbsp;&nbsp; 2. จัดทำเว็บไซต์ระบบโรงเคาะพ่นสีบริษัทธาราจำกัด<br><br>
                                <a href="http://projecthostingstudio.com/tara"><img src="img/plus.png" width="5%" height="5%"> คลิ๊กดูผลงานเพิ่มเติม...</a>
                                <a href="https://gitlab.com/Undermind08/tara_projecth.git
"><img src="img/download.png" width="5%" height="5%"> ดาว์นโหลดโค๊ด...</a>

                            </p>
                        </small></h4>
                    <h2 style="font-variant: normal; width: 100%; height: 100%;background-color: #413F3D; color:white;">ทักษะ</h2>
                    <h4><small style="font-size: 15px;">
                            <b>PHP</b> <div class="range-slider">
                                <div><input class="range-slider__range" type="range" value="65" min="0" max="100"></div>
                                <b>Adobe Photoshop</b>
                                <div><input class="range-slider__range" type="range" value="70" min="0" max="100"></div>
                                <b>HTML</b>
                                <div><input class="range-slider__range" type="range" value="80" min="0" max="100"></div>
                                <b>CSS</b>
                                <div><input class="range-slider__range" type="range" value="67" min="0" max="100"></div>

                            </div>
                            <br><br><br><br>
                        </small></h4>
                    <br>
            </div>

        </div>
    </div>

</div>


    </div>
</body>
</html>

<script src="{{ asset('js/bootstrap.js') }}"></script>
